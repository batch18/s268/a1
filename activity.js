db.course_bookings.insertMany([
	{"courseId": "C001", "studentId": "S004", "isCompleted": true},
	{"courseId": "C002", "studentId": "S001", "isCompleted": false},
	{"courseId": "C001", "studentId": "S003", "isCompleted": true},
	{"courseId": "C003", "studentId": "S002", "isCompleted": false},
	{"courseId": "C001", "studentId": "S002", "isCompleted": true},
	{"courseId": "C004", "studentId": "S003", "isCompleted": false},
	{"courseId": "C002", "studentId": "S004", "isCompleted": true},
	{"courseId": "C003", "studentId": "S007", "isCompleted": false},
	{"courseId": "C001", "studentId": "S005", "isCompleted": true},
	{"courseId": "C004", "studentId": "S008", "isCompleted": false},
	{"courseId": "C001", "studentId": "S013", "isCompleted": true}
	]);


db.course_bookings.aggregate([
			
				{$match: {"studentId" : "S013"}},
	            {$group: {_id : "studentId", courseId: {$sum:1}}}
		]);

db.course_bookings.aggregate([
			
				{$match: {"isCompleted" : false}},
	            {$project: { "courseId" : 0  }}
		]);

db.course_bookings.aggregate([
			
				{$sort: { "courseId" : -1,  "studentId" : 1 }}
		]);